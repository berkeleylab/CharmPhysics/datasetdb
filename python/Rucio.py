#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

from sys import exit

try:
    from rucio.client.didclient import DIDClient
    try:
        RucioDIDClient = DIDClient()
    except:
        exit('Failed to get DIDClient')
    from rucio.client.replicaclient import ReplicaClient
    try:
        RucioReplicaClient = ReplicaClient()
    except:
        exit('Failed to get ReplicaClient')
except:
    exit('Please first setup rucio')
