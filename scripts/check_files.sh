#!/bin/bash

# derivation to check
DERIVATION=$1

# make output dir
mkdir -p derivation_check

for group in `ls $LBNLCharm_DIR/data/DatasetDB/$DERIVATION`; do
    echo "~~ checking group: $group ~~"
    for ds in `cat $LBNLCharm_DIR/data/DatasetDB/$DERIVATION/$group`; do
        echo "~~~~" | tee -a derivation_check/$group;

        echo $ds | tee -a derivation_check/$group;
        rucio list-files $ds | grep -v "|" | grep -v "\+" 2>&1 | tee -a derivation_check/$group;

        if [[ $ds == *"p4416"* ]]; then
            echo `echo $ds | sed 's|p4416|p3970|g'` | tee -a derivation_check/$group;
            rucio list-files `echo $ds | sed 's|p4416|p3970|g'`| grep -v "|" | grep -v "\+" 2>&1 | tee -a derivation_check/$group;
        else
            echo `echo $ds | sed 's|p3970|p4416|g'` | tee -a derivation_check/$group;
            rucio list-files `echo $ds | sed 's|p3970|p4416|g'`| grep -v "|" | grep -v "\+" 2>&1 | tee -a derivation_check/$group;
        fi

        echo `echo $ds | sed 's|_p4416||g' | sed 's|_p3970||g' | sed 's|deriv.DAOD_'$DERIVATION'|recon.AOD|g'` | tee -a derivation_check/$group;
        rucio list-files `echo $ds | sed 's|_p4416||g' | sed 's|_p3970||g' | sed 's|deriv.DAOD_'$DERIVATION'|recon.AOD|g'` | grep -v "|" | grep -v "\+" 2>&1 | tee -a derivation_check/$group;        
    done
done
