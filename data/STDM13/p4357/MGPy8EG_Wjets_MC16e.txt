mc16_13TeV:mc16_13TeV.363600.MGPy8EG_N30NLO_Wenu_Ht0_70_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363601.MGPy8EG_N30NLO_Wenu_Ht0_70_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363602.MGPy8EG_N30NLO_Wenu_Ht0_70_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363603.MGPy8EG_N30NLO_Wenu_Ht70_140_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363604.MGPy8EG_N30NLO_Wenu_Ht70_140_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363605.MGPy8EG_N30NLO_Wenu_Ht70_140_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363606.MGPy8EG_N30NLO_Wenu_Ht140_280_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363607.MGPy8EG_N30NLO_Wenu_Ht140_280_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363608.MGPy8EG_N30NLO_Wenu_Ht140_280_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363609.MGPy8EG_N30NLO_Wenu_Ht280_500_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363610.MGPy8EG_N30NLO_Wenu_Ht280_500_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363611.MGPy8EG_N30NLO_Wenu_Ht280_500_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363612.MGPy8EG_N30NLO_Wenu_Ht500_700_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363613.MGPy8EG_N30NLO_Wenu_Ht500_700_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363614.MGPy8EG_N30NLO_Wenu_Ht500_700_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363615.MGPy8EG_N30NLO_Wenu_Ht700_1000_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363616.MGPy8EG_N30NLO_Wenu_Ht700_1000_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363617.MGPy8EG_N30NLO_Wenu_Ht700_1000_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363618.MGPy8EG_N30NLO_Wenu_Ht1000_2000_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363619.MGPy8EG_N30NLO_Wenu_Ht1000_2000_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363620.MGPy8EG_N30NLO_Wenu_Ht1000_2000_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363621.MGPy8EG_N30NLO_Wenu_Ht2000_E_CMS_CVetoBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363622.MGPy8EG_N30NLO_Wenu_Ht2000_E_CMS_CFilterBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363623.MGPy8EG_N30NLO_Wenu_Ht2000_E_CMS_BFilter.deriv.DAOD_STDM13.e4835_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363624.MGPy8EG_N30NLO_Wmunu_Ht0_70_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363625.MGPy8EG_N30NLO_Wmunu_Ht0_70_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363626.MGPy8EG_N30NLO_Wmunu_Ht0_70_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363627.MGPy8EG_N30NLO_Wmunu_Ht70_140_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363628.MGPy8EG_N30NLO_Wmunu_Ht70_140_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363629.MGPy8EG_N30NLO_Wmunu_Ht70_140_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363630.MGPy8EG_N30NLO_Wmunu_Ht140_280_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363631.MGPy8EG_N30NLO_Wmunu_Ht140_280_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363632.MGPy8EG_N30NLO_Wmunu_Ht140_280_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363633.MGPy8EG_N30NLO_Wmunu_Ht280_500_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363634.MGPy8EG_N30NLO_Wmunu_Ht280_500_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363635.MGPy8EG_N30NLO_Wmunu_Ht280_500_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363636.MGPy8EG_N30NLO_Wmunu_Ht500_700_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363637.MGPy8EG_N30NLO_Wmunu_Ht500_700_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363638.MGPy8EG_N30NLO_Wmunu_Ht500_700_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363639.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363640.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363641.MGPy8EG_N30NLO_Wmunu_Ht700_1000_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363642.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363643.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363644.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363645.MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_CVetoBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363646.MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_CFilterBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363647.MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_BFilter.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363648.MGPy8EG_N30NLO_Wtaunu_Ht0_70_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363649.MGPy8EG_N30NLO_Wtaunu_Ht0_70_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363650.MGPy8EG_N30NLO_Wtaunu_Ht0_70_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363651.MGPy8EG_N30NLO_Wtaunu_Ht70_140_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363652.MGPy8EG_N30NLO_Wtaunu_Ht70_140_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363653.MGPy8EG_N30NLO_Wtaunu_Ht70_140_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363654.MGPy8EG_N30NLO_Wtaunu_Ht140_280_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363655.MGPy8EG_N30NLO_Wtaunu_Ht140_280_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363656.MGPy8EG_N30NLO_Wtaunu_Ht140_280_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363657.MGPy8EG_N30NLO_Wtaunu_Ht280_500_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363658.MGPy8EG_N30NLO_Wtaunu_Ht280_500_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363659.MGPy8EG_N30NLO_Wtaunu_Ht280_500_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363660.MGPy8EG_N30NLO_Wtaunu_Ht500_700_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363661.MGPy8EG_N30NLO_Wtaunu_Ht500_700_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363662.MGPy8EG_N30NLO_Wtaunu_Ht500_700_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363663.MGPy8EG_N30NLO_Wtaunu_Ht700_1000_CVetoBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363664.MGPy8EG_N30NLO_Wtaunu_Ht700_1000_CFilterBVeto.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363665.MGPy8EG_N30NLO_Wtaunu_Ht700_1000_BFilter.deriv.DAOD_STDM13.e4944_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363666.MGPy8EG_N30NLO_Wtaunu_Ht1000_2000_CVetoBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363667.MGPy8EG_N30NLO_Wtaunu_Ht1000_2000_CFilterBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363668.MGPy8EG_N30NLO_Wtaunu_Ht1000_2000_BFilter.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363669.MGPy8EG_N30NLO_Wtaunu_Ht2000_E_CMS_CVetoBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363670.MGPy8EG_N30NLO_Wtaunu_Ht2000_E_CMS_CFilterBVeto.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357
mc16_13TeV:mc16_13TeV.363671.MGPy8EG_N30NLO_Wtaunu_Ht2000_E_CMS_BFilter.deriv.DAOD_STDM13.e5136_s3126_r10724_p4357